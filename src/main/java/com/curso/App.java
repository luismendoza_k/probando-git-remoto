package com.curso;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        System.out.println( "Hello World!" );

        //Validando parametros
        if (args.length>0){
            if(args[0].equalsIgnoreCase("listar")){
                for(int i=0;i<10;i++){
                    System.out.println(" Param : "+ i);
                }
            }else{
                System.out.println("Otro argumento");
            }
        }


        if(args.length>0){
            System.out.println("LLamada con parametros");
            for (String a : args){
                System.out.println(String.format("\t . '%s'",a));
            }
        }else{
            System.out.println("LLamada sin parametros");
        }

        //Esto es una nueva funcionalidad
        

        //Se creara una funcion de suma
        if(args.length>1){
            int suma = Integer.parseInt(args[0])  + Integer.parseInt(args[1]);
            System.out.println(" La suma de los numeros es : " + suma);
        }
        //Se creara una funcion de resta
        if(args.length>1){
            int suma = Integer.parseInt(args[0])  - Integer.parseInt(args[1]);
            System.out.println(" La suma de los numeros es : " + suma);
        }
    }
}
